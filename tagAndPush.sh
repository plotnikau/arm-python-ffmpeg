#!/bin/sh

git pull --ff-only && \
git tag v$(date +"%Y%m%d_%H%M%S") HEAD && git push origin --tags master
